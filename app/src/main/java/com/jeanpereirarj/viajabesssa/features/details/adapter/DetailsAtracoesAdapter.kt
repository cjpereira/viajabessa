package com.jeanpereirarj.viajabesssa.features.details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeanpereirarj.viajabesssa.R
import kotlinx.android.synthetic.main.adapter_details_list_item_comentarios.view.*

class DetailsAtracoesAdapter(private val str: List<String>) :
    RecyclerView.Adapter<DetailsAtracoesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DetailsAtracoesAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.adapter_details_list_item_atracoes, parent, false)
        )
    }

    override fun onBindViewHolder(holder: DetailsAtracoesAdapter.ViewHolder, position: Int) {
        val str = str[position]
        holder.bindData(str)
    }

    override fun getItemCount() = str.size

    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        fun bindData(atracoes: String) {
            view?.comentario.text = atracoes
        }
    }

}
