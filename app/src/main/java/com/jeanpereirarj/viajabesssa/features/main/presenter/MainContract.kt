package com.jeanpereirarj.viajabesssa.features.main.presenter

import com.jeanpereirarj.viajabesssa.base.BaseMvpPresenter
import com.jeanpereirarj.viajabesssa.base.BaseMvpView

object MainContract {

    interface View : BaseMvpView {}

    interface Presenter : BaseMvpPresenter<View> {
        fun getAll()
    }

}