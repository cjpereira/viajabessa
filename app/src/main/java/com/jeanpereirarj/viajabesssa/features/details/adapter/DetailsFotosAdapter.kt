package com.jeanpereirarj.viajabesssa.features.details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeanpereirarj.viajabesssa.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_details_list_item_fotos.view.*

class DetailsFotosAdapter(private val urls: List<String>) :
    RecyclerView.Adapter<DetailsFotosAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.adapter_details_list_item_fotos, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val url = urls[position]
        holder.bindData(url)
    }

    override fun getItemCount() = urls.size

    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        fun bindData(url: String) {
            Picasso.get()
                .load(url)
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_unavailable)
                .into(view.image)

        }
    }
}