package com.jeanpereirarj.viajabesssa.features.details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeanpereirarj.viajabesssa.R
import kotlinx.android.synthetic.main.adapter_details_list_item_servicos.view.*

class DetailsServicosAdapter(private val str: List<String>) :
    RecyclerView.Adapter<DetailsServicosAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.adapter_details_list_item_servicos, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val servico = str[position]
        holder.bindData(servico)
    }

    override fun getItemCount() = str.size

    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        fun bindData(servico: String) {
            view.servico.text = servico

        }
    }

}