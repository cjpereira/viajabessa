package com.jeanpereirarj.viajabesssa.utils;

public class Constants {

    public static final String MSG_SYSTEM_NOT_AVAILABLE = "Sistema indisponível no momento, tente mais tarde!";
    public static final String MSG_CONNECTION_NOT_AVAILABLE = "Problema com a conexão. Tente novamente.";

    public static final String PARAM_PACOTE = "key_pacote";


    public static final long TIME_OUT = 60;

}
