package com.jeanpereirarj.viajabesssa.service

import com.jeanpereirarj.viajabesssa.BuildConfig
import com.jeanpereirarj.viajabesssa.api.URLBase
import com.jeanpereirarj.viajabesssa.payload.PacoteResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

@URLBase(BuildConfig.API_URL)
interface IPacoteService {

    @GET("travel")
    fun getAll(): Call<List<PacoteResponse>>

    @GET("packages/{packagename}")
    fun getByPacotename(@Path("packagename") username: String): Call<PacoteResponse>
}