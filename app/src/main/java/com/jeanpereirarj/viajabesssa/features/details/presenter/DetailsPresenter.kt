package com.jeanpereirarj.viajabesssa.features.details.presenter

import com.jeanpereirarj.viajabesssa.base.BaseMvpPresenterImpl

open class DetailsPresenter : BaseMvpPresenterImpl<DetailsContract.View>(),
    DetailsContract.Presenter {

}