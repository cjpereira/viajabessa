package com.jeanpereirarj.viajabesssa.features.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeanpereirarj.viajabesssa.R
import com.jeanpereirarj.viajabesssa.domain.Pacote
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_main_list_item.view.*
import kotlinx.android.synthetic.main.info.view.*


class MainAdapter(onItemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    val onItemClickListener: OnItemClickListener = onItemClickListener

    var pacotes: List<Pacote>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.adapter_main_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MainAdapter.ViewHolder, position: Int) {
        val pacote = pacotes?.get(position)
        holder.bindData(pacote!!)
        holder.itemView.setOnClickListener { view ->
            onItemClickListener.onItemClicked(pacote)
        }
    }

    override fun getItemCount() = pacotes?.size!!

    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        fun bindData(pacote: Pacote) {
            view.titulo.text = pacote.titulo
            view.subtitulo.text = pacote.subtitulo
            view.extra.text = pacote.extra
            view.valor.text = pacote.valor
            view.pontuacao.text = pacote.pontuacao

            Picasso.get()
                .load(pacote.imagens?.get(0))
                .placeholder(R.drawable.ic_icone_ref_destino)
                .error(R.drawable.ic_icone_ref_destino)
                .into(view.image)

        }
    }

}


interface OnItemClickListener {
    fun onItemClicked(pacote: Pacote)
}