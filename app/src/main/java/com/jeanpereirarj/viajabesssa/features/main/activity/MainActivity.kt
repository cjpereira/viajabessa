package com.jeanpereirarj.viajabesssa.features.main.activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.jeanpereirarj.viajabesssa.R
import com.jeanpereirarj.viajabesssa.base.BaseActivity
import com.jeanpereirarj.viajabesssa.domain.Pacote
import com.jeanpereirarj.viajabesssa.features.details.activity.DetailsActivity
import com.jeanpereirarj.viajabesssa.features.main.adapter.MainAdapter
import com.jeanpereirarj.viajabesssa.features.main.adapter.OnItemClickListener
import com.jeanpereirarj.viajabesssa.features.main.presenter.MainContract
import com.jeanpereirarj.viajabesssa.features.main.presenter.MainPresenter
import com.jeanpereirarj.viajabesssa.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity<MainContract.View, MainContract.Presenter>(),
    MainContract.View {

    override var mPresenter: MainContract.Presenter = MainPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler_view.layoutManager = LinearLayoutManager(this@MainActivity)

        var adapter = MainAdapter(object : OnItemClickListener {
            override fun onItemClicked(pacote: Pacote) {
                startActivity(
                    Intent(this@MainActivity, DetailsActivity::class.java).putExtra(
                        Constants.PARAM_PACOTE, pacote
                    )
                )
            }
        })

        mPresenter.getAll()
        recycler_view.adapter = adapter
    }

    override fun success(result: Any) {
        (recycler_view.adapter as MainAdapter).pacotes = result as List<Pacote>
        (recycler_view.adapter as MainAdapter).notifyDataSetChanged()
    }

}