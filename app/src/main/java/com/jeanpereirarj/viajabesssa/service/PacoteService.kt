package com.jeanpereirarj.viajabesssa.service

import com.jeanpereirarj.viajabesssa.api.ApiException
import com.jeanpereirarj.viajabesssa.api.ApiManager
import com.jeanpereirarj.viajabesssa.domain.Pacote
import com.jeanpereirarj.viajabesssa.payload.PacoteResponse
import com.jeanpereirarj.viajabesssa.utils.Constants
import retrofit2.Response
import rx.Emitter
import rx.Observable


class PacoteService {

    fun getAll(): Observable<ArrayList<Pacote>> {

        return Observable.create({ emitter ->
            val service = ApiManager.createRetrofit(IPacoteService::class.java)
            try {

                var response: Response<List<PacoteResponse>> = service.getAll().execute()

                if (response.errorBody() == null) {

                    var pacoteList = ArrayList<Pacote>()
                    for (pacotePayload in response.body()!!) {
                        pacoteList.add(pacotePayload.toModel())
                    }
                    emitter.onNext(pacoteList)
                    emitter.onCompleted()
                } else {
                    //aqui fazer o tratamento de erro
                    throw Exception()
                }

            } catch (e: Exception) {
                if (e is ApiException) {
                    emitter.onError(e)
                } else {
                    emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                }
            }
        }, Emitter.BackpressureMode.LATEST)
    }

}