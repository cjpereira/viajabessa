package com.jeanpereirarj.viajabesssa.features.details.activity

import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Bundle
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.jeanpereirarj.viajabesssa.R
import com.jeanpereirarj.viajabesssa.base.BaseActivity
import com.jeanpereirarj.viajabesssa.domain.Pacote
import com.jeanpereirarj.viajabesssa.features.details.adapter.DetailsComentariosAdapter
import com.jeanpereirarj.viajabesssa.features.details.presenter.DetailsContract
import com.jeanpereirarj.viajabesssa.features.details.presenter.DetailsPresenter
import com.jeanpereirarj.viajabesssa.utils.Constants
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.info.*

class DetailsActivity : BaseActivity<DetailsContract.View, DetailsContract.Presenter>(),
    DetailsContract.View, OnMapReadyCallback {

    override var mPresenter: DetailsContract.Presenter = DetailsPresenter()

    private lateinit var googleMap: GoogleMap
    private val LOCATION_PERMISSION_REQUEST_CODE = 1
    lateinit var pacote: Pacote

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        if (intent.getSerializableExtra(Constants.PARAM_PACOTE) != null) {
            pacote = intent.getSerializableExtra(Constants.PARAM_PACOTE) as Pacote
        }

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        updateLayout()

        comentarios.layoutManager = LinearLayoutManager(this@DetailsActivity)
        var adapter = DetailsComentariosAdapter(pacote.comentarios!!)
        comentarios.adapter = adapter

    }

    fun updateLayout() {

        val actionbar = supportActionBar
        actionbar!!.title = getString(R.string.str_detalhes)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayShowHomeEnabled(true)
        actionbar.elevation = 10f

        titulo.text = pacote.titulo!!
        subtitulo.text = pacote.subtitulo!!
        extra.text = pacote.extra!!
        valor.text = pacote.valor!!
        pontuacao.text = pacote.pontuacao!!

    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        setUpMap()
        this.googleMap = googleMap

        val myParkingMarker = MarkerOptions()
            .position(pacote.getLatLng())
            .visible(true)
            .title("Destino")
            .snippet(titulo.toString())
            .rotation(3.5f)
            .icon(vectorToBitmap(resources, R.drawable.ic_icone_ref_destino))
            .anchor(0.5f, 0.5f)

        this.googleMap.addMarker(myParkingMarker)

        var circleOptions: CircleOptions = CircleOptions()
        circleOptions.center(pacote.getLatLng())
        circleOptions.radius(150.0)
        circleOptions.strokeColor(Color.TRANSPARENT)
        circleOptions.fillColor(0x220000FF)
        circleOptions.strokeWidth(5f)
        this.googleMap.addCircle(circleOptions)

        val cameraPosition =
            CameraPosition.builder().target(pacote.getLatLng()).tilt(20F).zoom(13F).bearing(90F)
                .build()
        this.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

    private fun vectorToBitmap(res: Resources, @DrawableRes id: Int): BitmapDescriptor {
        return vectorToBitmap(res, id, 0)
    }

    /**
     * Demonstrates converting a [Drawable] to a [BitmapDescriptor],
     * for use as a marker icon.
     */
    private fun vectorToBitmap(
        res: Resources,
        @DrawableRes id: Int,
        @ColorInt color: Int
    ): BitmapDescriptor {
        val vectorDrawable = ResourcesCompat.getDrawable(res, id, null)
        val bitmap = Bitmap.createBitmap(
            vectorDrawable!!.getIntrinsicWidth(),
            vectorDrawable?.getIntrinsicHeight()!!, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
        if (color != 0)
            vectorDrawable?.let { DrawableCompat.setTint(it, color) }

        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }


}