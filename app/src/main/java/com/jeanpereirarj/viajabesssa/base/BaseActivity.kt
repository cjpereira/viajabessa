package com.jeanpereirarj.viajabesssa.base

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.jeanpereirarj.viajabesssa.R


abstract class BaseActivity<in V : BaseMvpView, T : BaseMvpPresenter<V>>
    : AppCompatActivity(), BaseMvpView {

    private val TAG = this::class.java.simpleName

    override fun getRootContext(): Context = this
    protected abstract var mPresenter: T
    var progress: ProgressDialog? = null
    var dialogBar: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attachView(this as V)
        Log.d(TAG, "onCreate")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")

    }

    override fun onPostResume() {
        super.onPostResume()
        Log.d(TAG, "onPostResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
        mPresenter.detachView()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showMessage(message: String?) {

        hideLoading()
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage(message)
        alertDialogBuilder.setTitle("")
        alertDialogBuilder
            .setCancelable(false)
            .setNegativeButton(resources.getString(R.string.dialog_neutral_button)) { _, _ ->

            }

        val alertD = alertDialogBuilder.create()
        alertD.show()
    }

    fun loading() {
        progress = ProgressDialog(this)
        progress?.setCancelable(false)
        progress?.setTitle("")
        progress?.setMessage(this.getString(R.string.text_loading))
        progress?.show()
    }

    fun loading(message: String) {

        progress = ProgressDialog(this)
        progress?.setCancelable(false)
        progress?.setTitle("")
        progress?.setMessage(message)
        progress?.show()

    }

    fun hideLoading() {
        if (progress != null) {
            progress?.dismiss()
            progress?.cancel()
        }
    }

    override fun success(result: Any) {
    }

}
