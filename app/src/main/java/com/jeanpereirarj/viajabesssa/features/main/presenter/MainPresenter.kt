package com.jeanpereirarj.viajabesssa.features.main.presenter

import com.jeanpereirarj.viajabesssa.base.BaseMvpPresenterImpl
import com.jeanpereirarj.viajabesssa.service.PacoteService
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

open class MainPresenter : BaseMvpPresenterImpl<MainContract.View>(),
    MainContract.Presenter {

    override fun getAll() {
        PacoteService().getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                mView?.success(result)
            },
                { throwable ->
                    mView?.showMessage(throwable.message)
                }
            )
    }


}