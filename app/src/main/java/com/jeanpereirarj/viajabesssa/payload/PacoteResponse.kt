package com.jeanpereirarj.viajabesssa.payload

import com.google.gson.annotations.SerializedName
import com.jeanpereirarj.viajabesssa.domain.Pacote
import java.io.Serializable

class PacoteResponse : Serializable {

    @SerializedName("titulo")
    val titulo: String? = null

    @SerializedName("subtitulo")
    val subtitulo: String? = null

    @SerializedName("extra")
    val extra: String? = null

    @SerializedName("pontuacao")
    val pontuacao: String? = null

    @SerializedName("valor")
    val valor: String? = null

    @SerializedName("servicos")
    val servicos: ArrayList<String>? = null

    @SerializedName("atracoes")
    val atracoes: ArrayList<String>? = null

    @SerializedName("imagens")
    val imagens: ArrayList<String>? = null

    @SerializedName("comentarios")
    val comentarios: ArrayList<String>? = null

    @SerializedName("localizacao")
    val localizacao: ArrayList<String>? = null

    fun toModel(): Pacote {
        return Pacote(
            this.titulo,
            this.subtitulo,
            this.extra,
            this.pontuacao,
            this.valor,
            this.servicos,
            this.atracoes,
            this.imagens,
            this.comentarios,
            this.localizacao
        )
    }
}

