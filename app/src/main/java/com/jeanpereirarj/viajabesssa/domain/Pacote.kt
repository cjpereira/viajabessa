package com.jeanpereirarj.viajabesssa.domain

import com.google.android.gms.maps.model.LatLng
import java.io.Serializable

class Pacote : Serializable {

    var titulo: String? = null
    var subtitulo: String? = null
    var extra: String? = null
    var pontuacao: String? = null
    var valor: String? = null
    var servicos: ArrayList<String>? = null
    var atracoes: ArrayList<String>? = null
    var imagens: ArrayList<String>? = null
    var comentarios: ArrayList<String>? = null
    var localizacao: ArrayList<String>? = null

    constructor(
        titulo: String?,
        subtitulo: String?,
        extra: String?,
        pontuacao: String?,
        valor: String?,
        servicos: ArrayList<String>?,
        atracoes: ArrayList<String>?,
        imagens: ArrayList<String>?,
        comentarios: ArrayList<String>?,
        localizacao: ArrayList<String>?
    ) {
        this.titulo = titulo
        this.subtitulo = subtitulo
        this.extra = extra
        this.pontuacao = pontuacao
        this.valor = valor
        this.servicos = servicos
        this.atracoes = atracoes
        this.imagens = imagens
        this.comentarios = comentarios
        this.localizacao = localizacao
    }

    fun getLatLng(): LatLng {
        return LatLng(localizacao?.get(0)?.toDouble()!!, localizacao?.get(1)?.toDouble()!!)
    }
}