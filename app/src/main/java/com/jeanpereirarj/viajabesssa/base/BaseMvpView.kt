package com.jeanpereirarj.viajabesssa.base

import android.content.Context

interface BaseMvpView {
    fun getRootContext(): Context
    fun showMessage(error: String?)
    fun success(result: Any)
}