package com.jeanpereirarj.viajabesssa.features.details.presenter

import com.jeanpereirarj.viajabesssa.base.BaseMvpPresenter
import com.jeanpereirarj.viajabesssa.base.BaseMvpView

object DetailsContract {

    interface View : BaseMvpView {}

    interface Presenter : BaseMvpPresenter<View> {}

}