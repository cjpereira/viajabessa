package com.jeanpereirarj.viajabesssa.base

interface BaseMvpPresenter<in V : BaseMvpView> {
    fun attachView(view: V)
    fun detachView()
}